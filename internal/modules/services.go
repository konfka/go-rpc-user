package modules

import (
	"gitlab.com/konfka/go-rpc-user/internal/infrastructure/component"
	uservice "gitlab.com/konfka/go-rpc-user/internal/modules/user/service"
	"gitlab.com/konfka/go-rpc-user/internal/storages"
)

type Services struct {
	User          uservice.Userer
	UserClientRPC uservice.Userer
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
	}
}
