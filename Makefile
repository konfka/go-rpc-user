PHONY: generate-structs
generate-structs:
	mkdir -p pkg/user
	protoc --go_out=pkg/user --go_opt=paths=source_relative \
			rpc/user/grpc/proto_user.proto